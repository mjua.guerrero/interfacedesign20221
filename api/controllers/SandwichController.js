/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
  make: (req, res) => {
    return res.view('sandwich/makeASandwich');
  }
};

