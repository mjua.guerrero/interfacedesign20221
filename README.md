# Interface Design Project 2022-1

A [Sails v1](https://sailsjs.com) application


## Content 

- [What the hell it's Sails JS?](#What the hell is Sails JS?)
- [Version Information](#version information)
- [Requirements](#requirements)
- [Launch the project](#launch the project)
- [Configuration](#configuration)
- [What is MVC pattern?](#mvc)
- [Controllers](#controllers)


## What the hell it's Sails JS?

Sails JS it's a small and easy going framework based on the MVC architecture created by [Mike McNeil](https://github.com/mikermcneil) focused on build Backend system, but you can use it to create cool websites at all.

### Links for learning Sails Js

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)


### Version info

This app was originally generated on Sat Oct 16 2021 21:23:52 GMT-0500 (Central Daylight Time) using Sails v1.2.4.


## Requirements

| Technology | Version                                                               | Release Date  |
|------------|-----------------------------------------------------------------------|---------------|
| Node Js    | [`14.17.0`](https://nodejs.org/en/blog/release/v14.17.0/)             |  2021 May 11  |
| Sails Js   | [`1.2.4`](https://github.com/balderdashy/sails/releases/tag/v1.2.4)   |  2020 Jul 14  | 
| NPM        | [`6.14.13`](https://www.npmjs.com/package/npm/v/6.14.13)              |  2021 Apr 12  |
| NVM *      | [`0.35.0`](https://github.com/nvm-sh/nvm/releases/tag/v0.35.0)        |  2019 Oct 01  |


* An optional Node Version Manager


## Lauch the project

1. Clone the repository

2. Move to the project folder

3. Install required dependencies:

> $ nvm use <br>
> $ npm ci

4. Into the project folder run the `cli` of `sails`:

> $ sails lift

 this will run the project. You need to install Sails globally. To do this, run:

> $ npm install sails@1.x.x -g  (Depends on the version you decided to use)


## Configuration

Not required for the moment


## What is MVC pattern?

MVC (Model-View-Controller) is a pattern in software design commonly used to implement user interfaces, data, and controlling logic. It emphasizes a separation between the software’s business logic and display. This "separation of concerns" provides for a better division of labor and improved maintenance.

### If you wanna learn more about it:

+ [MVC Architecture](https://developer.chrome.com/docs/apps/app_frameworks/) 
+ [MVC](https://developer.mozilla.org/en-US/docs/Glossary/MVC)
+ [Everything you need to know about MVC architecture](https://towardsdatascience.com/everything-you-need-to-know-about-mvc-architecture-3c827930b4c1)
+ [The Model View Controller Pattern](https://www.freecodecamp.org/news/the-model-view-controller-pattern-mvc-architecture-and-frameworks-explained/)
+ [What is MVC, really?](https://softwareengineering.stackexchange.com/questions/127624/what-is-mvc-really)


## Controllers

For simpler projects and prototypes, often the quickest way to get started writing Sails apps is to organize your actions into controller files.

There are two ways to create a controller.

- If you just want to create a new controller, type:

> sails generate controller `controllername`

You will see a new file created into the `api/controllers` folder with the camel case notation. Awesome, is not? 


- If you want to create a controller and the model associated to, type:

> sails generate api `controllername` <br> or <br>

You will see a new file created into the `api/controllers` and a new model into the folder `api/models`. Amazing, is not?


Note: You can manually create the file in the `api/controllers` and it will works as well.